// needs common header and cors headers
let express = require("express");
let bodyParser = require("body-parser");

let app = express();
let http_server, http_serverconfig, apis, guard;

// config server
let set_http_configs = function(http_configs = 3008) {
  if (typeof http_configs != "object") {
    let port = parseInt(http_configs);
    if (!port) throw new Error("object or port as input!!!");
    http_serverconfig = {
      protocal: "http", //'https','http',
      host: "",
      port,
    };
  } else {
    http_serverconfig = {
      protocal: http_configs.protocal || "http", //'https','http',
      host: http_configs.host || "",
      port: http_configs.port || 3008,
      https_options: http_configs.https_options,
    };
  }

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));

  // common headers
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:8080");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
  });
};

// config api files
let set_files = function(apislist) {
  if (!apislist || (Array.isArray(apislist) && !apislist.length))
    apislist = [{ name: "api", path: "./api" }];
  if (!Array.isArray(apislist)) apislist = [apislist];
  apis = apislist.map(api => {
    if (typeof api == "string") {
      let a = api.split("/");
      let name = a[a.length - 1];
      let prefix = apislist.length > 1 ? name : "";
      return {
        path: api,
        prefix,
        name: a[a.length - 1],
      };
    } else {
      api.name = api.name || "";
      api.prefix = api.prefix || "";
      return api;
    }
  });
  apis.sort((a, b) => (a.prefix > b.prefix ? -1 : 1));
};

//set guard
let set_gaurd = function(user_guard) {
  //returns {haspermission:boolean,result{user:{*},httpstatus:number,body:[{message}]}}
  guard = user_guard;
};
// starting http server
let http_start = function(options) {
  if (!http_serverconfig) {
    set_http_configs();
    set_files();
  }
  //routes
  if (!options || !options.withoutroute) {
    app = require("./http")(app, apis, guard);
  }

  // http or https server
  if (http_serverconfig.protocal == "https") {
    http_server = require("https").createServer(
      http_serverconfig.https_options,
      app
    );
  } else {
    http_server = require("http").createServer(app);
  }

  try {
    http_server.close();
    http_server.listen(
      http_serverconfig.port,
      http_serverconfig.host,
      function() {
        console.log(
          "Server started on \n",
          (http_serverconfig.protocal == "https" ? "https://" : "http://") +
            (http_serverconfig.host || "localhost") +
            ":" +
            http_serverconfig.port
        );
      }
    );
  } catch (ex) {
    console.log(ex);
  }
};

//starting socketio server
let socketio_start = function() {
  if (!http_server) http_start({ withoutroute: true });
  // Set up Socket.io
  var io = require("./socket");
  io.init(http_server, apis, guard);
};

/**
 *
 * @param {string|number|{http_config:{}|number ,api:string[]|{prefix:string,name:string,path:string}[],HttpServer:boolean,socketioServer:boolean}} configs
 * @param {function} stguard guard for token and each path and
 */
let start = function(configs = {}, stguard) {
  if (!http_serverconfig) {
    if (typeof configs !== "object") {
      set_http_configs(configs);
      configs = {};
    } else {
      set_http_configs(configs.http_config);
    }
  }
  if (!apis) {
    set_files(configs.api);
  }
  if (!guard) {
    set_gaurd(
      stguard || (() => ({ haspermission: true, result: { user: {} } }))
    );
  }

  if (configs.HttpServer == undefined || configs.HttpServer) {
    http_start();
  }

  if (configs.socketioServer == undefined || configs.socketioServer) {
    socketio_start();
  }
};

module.exports = {
  start,
  set_files,
  set_http_configs,
  set_gaurd,
  http_start,
  socketio_start,
};
