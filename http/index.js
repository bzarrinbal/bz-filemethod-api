/**
 * gets app and file list and creates routes
 */
module.exports = function(app, apis, guard) {
  var routes = require("./routes")(apis, guard);
  app.use(routes);
  return app;
};
