module.exports = function(path) {
  var { filemethod } = require("../../methodfile/filemehtods")(path);
  return async (req, res) => {
    result = await filemethod(req.params.filename, req.params.method, req.body);
    res.status(result.httpstatus).send(result.body);
  };
};
