var express = require("express");
var router = express.Router();
var controller = require("../controllers");

module.exports = function(apis, guard) {
  var http_guard = async function(req, res, next) {
    let permit = await guard(req.headers.api_token, req.originalUrl);
    if (permit.haspermission) {
      req.this_user = permit.result.user;
      return next();
    } else {
      return res
        .status((permit.result || {}).httpstatus || 403)
        .send((permit.result || {}).body || [{ message: "Not Authorized" }]);
    }
  };
  for (let i = 0; i < apis.length; i++) {
    let api = apis[i];
    if (api.prefix) {
      router.post(
        "/" + api.prefix + "/:filename/:method",
        http_guard,
        controller(api.path)
      );
    } else {
      router.post("/:filename/:method", http_guard, controller(api.path));
    }
  }

  return router;
};
