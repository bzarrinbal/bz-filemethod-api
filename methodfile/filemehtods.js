module.exports = function(filepath) {
  var path = require("path");
  var files = {},
    filemethodnames = {};
  require("fs")
    .readdirSync(path.resolve(path.dirname(require.main.filename), filepath), {
      withFileTypes: true,
    })
    .filter(fl => fl.isDirectory && !fl.isDirectory())
    .forEach(function(file) {
      filearray = file.name.split(".");
      filearray.pop();
      finfile = filearray.join(".");
      if (finfile.slice(0, 1) != "_") {
        files[finfile] = require(path.resolve(
          path.dirname(require.main.filename),
          filepath,
          finfile
        ));
        filemethodnames[finfile] = Object.keys(files[finfile]);
      }
    });

  var filemethod = async (file, method, data) => {
    return new Promise(async resolve => {
      if (!files[file] || !files[file][method]) {
        return resolve({
          httpstatus: 404,
          body: [{ message: "Path doesn't exist" }],
        });
      }
      try {
        func = files[file][method];
        respond = await func(data);
        if (respond.httpstatus) return resolve(respond);
        return resolve({ httpstatus: 200, body: respond });
      } catch (err) {
        if (!(err || {}).httpstatus)
          err = { httpstatus: 500, body: [{ message: "server Error" }] };
        return resolve(err);
      }
    });
  };

  return {
    filemethod,
    filemethodnames,
  };
};
