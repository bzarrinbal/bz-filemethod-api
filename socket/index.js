var io;

module.exports = {
  init: function(server, apis, guard) {
    io = require("socket.io")(server);
    // connection permission
    // io.set("authorization", function(handshake, callback) {
    //   // console.log(handshake._query); //get query from client
    //   callback(null, true);
    // });

    io.on("connection", socket => {
      // get token from query
      let token = (socket.request._query || {}).api_token;
      for (let i = 0; i < apis.length; i++) {
        let api = apis[i];
        var {
          filemethodnames,
          filemethod,
        } = require("../methodfile/filemehtods")(api.path);
        setsocketlisteners(
          socket,
          filemethodnames,
          filemethod,
          token,
          guard,
          api.prefix
        );
      }
    });
  },
  io,
};

/**
 *
 * @param {*} socket -> instance of socket
 * @param {*} filelist -> object with filename as key and array of method as value
 */
var setsocketlisteners = (
  socket,
  filelist,
  filemethod,
  token,
  guard,
  prefix
) => {
  Object.keys(filelist).forEach(file => {
    filelist[file].forEach(async method => {
      let ev_path = (prefix ? prefix + "/" : "") + file + "/" + method;
      let permit = await guard(token, ev_path);
      if (!permit || !permit.haspermission) {
        socket.on(ev_path, async data => {
          return socket.emit("result:" + ev_path, {
            httpstatus: (permit.result || {}).httpstatus || 403,
            body: (permit.result || {}).body || [{ message: "Not Authorized" }],
          });
        });
      } else {
        socket.on(ev_path, async data => {
          result = await filemethod(file, method, data);
          return socket.emit("result:" + ev_path, result);
        });
      }
    });
  });
};
